# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qmake [ slot=5 ] gtk-icon-cache
require python [ blacklist=2 multibuild=false ]

export_exlib_phases src_prepare src_install

SUMMARY="Python library and command line tool for configuring a YubiKey"

HOMEPAGE="https://developers.yubico.com/${PN}/"
DOWNLOADS="${HOMEPAGE}Releases/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/Release_Notes.html"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-crypt/yubikey-manager[python_abis:*(-)?]
        dev-python/pyotherside[python_abis:*(-)?]
        x11-libs/qtbase:5
        x11-libs/qtdeclarative:5
    run:
        x11-libs/qtquickcontrols2:5
"

BUGS_TO="heirecka@exherbo.org"

WORK="${WORKBASE}"

EQMAKE_PARAMS=( PYTHON3_BINARY_NAME="${PYTHON}" )

yubikey-manager-qt_src_prepare() {
    default

    edo sed -e "s:/usr/bin:/usr/$(exhost --target)/bin:" \
            -i ykman-gui/deployment.pri
}

yubikey-manager-qt_src_install() {
    default

    insinto /usr/share/applications
    doins resources/ykman-gui.desktop

    insinto /usr/share/icons/hicolor/128x128/apps
    doins resources/icons/ykman.png
}

