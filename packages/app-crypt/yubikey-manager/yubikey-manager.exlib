# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools ]

SUMMARY="Python library and command line tool for configuring a YubiKey"

HOMEPAGE="https://developers.yubico.com/${PN}/"
DOWNLOADS="${HOMEPAGE}Releases/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}Release_Notes.html"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/click[python_abis:*(-)?]
        dev-python/cryptography[python_abis:*(-)?]
        dev-python/fido2[python_abis:*(-)?]
        dev-python/pyopenssl[python_abis:*(-)?]
        dev-python/pyscard[python_abis:*(-)?]
        dev-python/pyusb[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        python_abis:2.7? ( dev-python/enum34[python_abis:2.7] )
    run:
        app-crypt/ccid
        sys-auth/yubikey-personalization
    test:
        python_abis:2.7? ( dev-python/mock[python_abis:2.7] )
"

BUGS_TO="heirecka@exherbo.org"

